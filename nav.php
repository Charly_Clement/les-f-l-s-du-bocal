<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="https://kit.fontawesome.com/427a026171.js"></script>
    <title>Les Fêlés du Bocal</title>
</head>

<body>

    <div class="container-fluid ">
        <div class="row align-items-center fond-beige py-4">
            <div class="col-4 offset-2">
                <a href="index.php"><img class="w-25" src="items/logo.png" alt="Logo Bocal"></a>
            </div>
            <div>
                <nav>
                    <ul class="nav justify-content-center my-auto">
                        <li class="nav-item">
                            <a href="#" class="nav-link text-dark ml-2">CONCEPT</a>
                        </li>
                        <li class="nav-item">
                            <a href="bocaux.php" class="nav-link text-dark ml-2" href="#">PLATS</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link text-dark ml-2" href="#">HORAIRES</a>
                        </li>
                        <li class="nav-item">
                            <a href="contact.php" class="nav-link text-dark ml-2">CONTACT</a>
                        </li>
                        <li class="nav-item je-commande">
                            <a href="#" class="nav-link text-white"><i class="fas fa-phone-alt"></i> JE COMMANDE</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

