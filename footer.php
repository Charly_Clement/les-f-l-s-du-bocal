
    <div class="container-fluid mt-1">
        <div class="row fond-beige pt-5 pb-3">
            <div class="col-2 text-center">
                <div class="tel-footer-gauche">
                    <i class="fas fa-phone-alt fa-2x mt-2"></i>
                </div>
                <div>
                    <p>JE COMMANDE<br> PAR TÉLÉPHONE</p>
                </div>
            </div><!-- / col-2 -->
            <div class="col-2">
                <a href="index.php"><img class="w-50 mb-5" src="items/logo.png" alt="Logo Bocal"></a>
                    <div class="align-content">
                        <div>
                        <p>Suivez-nous sur : </p>
                        </div>
                        <div class="row">
                        <i class="fab fa-instagram ml-3"></i>
                        <i class="fab fa-facebook-square ml-3"></i>
                        </div>
                    </div>
            </div><!-- / col-2 -->
            <div class="col-3 text-center">
                <p>02.43.78.25.11<br/>
                    contact@les-feles-du-bocal.bio<br/><br/>
                    198 rue Nationale<br/>
                    Place Washington<br/>
                    72000 Le Mans</p>
                <a class="text-dark" href="mentions_legales.php"><b>Mentions Légales</b></a>
            </div>
            <ul class="col-2 list-unstyled text-center">
                <!-- Rajouter les pages en lien -->
                <li>
                    <a class="text-dark" href="#">Concept</a>
                </li>
                <li>
                    <a class="text-dark" href="#">Plat du jour</a>
                </li>
                <li>
                    <a class="text-dark" href="#">Palette de saveurs</a>
                </li>
                <li>
                    <a class="text-dark" href="#">Horaires</a>
                </li>
                <li>
                    <a class="text-dark" href="#">Contact</a>
                </li>
                <li class="je-commande mt-3">
                    <a href="#" class="nav-link text-white"><i class="fas fa-phone-alt"></i> JE COMMANDE</a>
                </li>
            </ul>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>