<?php

session_start();

if (!isset($_SESSION["logged_in"])) { 
    header ("Location: login.php");
}

include "config2.php";

if (isset($_POST['submit'])){
    $req = $bdd->prepare("SELECT * FROM avis ORDER BY date_creation");
    $req->execute();
    $results = $req->fetchALL();
    foreach ($results as $avis) {
        $recupVisibilite = isset($_POST['visibilite_'.$avis['id_avis']]) && !empty($_POST['visibilite_'.$avis['id_avis']]) ? $_POST['visibilite_'.$avis['id_avis']]: "non";
            $req = $bdd->prepare("UPDATE avis SET visibilite=? WHERE id_avis=?"); 
            $req->execute([$recupVisibilite, intval($avis['id_avis'])]);
    }   
}

include "nav.html";

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <form role="form" method="post">
            <h1 class="m-0 text-dark">Liste des avis</h1>
            <button type="submit" name="submit" class="btn btn-primary">Envoyer les avis cochés sur le site</button>
          </div><!-- /.col -->
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="row">

        <!-- Small boxes (Stat box) -->
        <?php
        $req = $bdd->prepare("SELECT * FROM avis ORDER BY date_creation DESC");
        $req->execute();
        $results = $req->fetchALL();
        foreach ($results as $avis) {
        ?>

            <div class="card" style="width: 31rem; margin: 10px;">
            <div class="card-body">
                <h5 class=""><?php echo $avis["nom_prenom"]; ?>&nbsp;&nbsp;<input type="checkbox" name="visibilite_<?php echo $avis['id_avis'];?>" value="oui"></h5>
                <h6 class="card-subtitle mb-2 text-muted"><?php echo $avis["date_creation"]; ?></h6>
                <p class="card-text"><?php echo $avis["texte"]; ?></p>
                <a href="modifier_avis.php?id=<?php echo $avis["id_avis"] ?>" class="btn btn-success">Modifier</a>
            </div>
            </div>

        <?php }?>
        </form>
        </div>
        <!-- /.row (main row) -->

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php

include "footer.html";

?>
