<?php

session_start();

include "config2.php";

$recupPseudo = isset($_POST['pseudo']) && !empty($_POST['pseudo']) ? $_POST['pseudo']: "";
$recupMdp = isset($_POST['mdp']) && !empty($_POST['mdp']) ? $_POST['mdp']: "";

$identifiantInvalid = null;

if (isset($_POST['submit'])){
    if (isset($_POST['pseudo']) && !empty($_POST['pseudo']) && isset($_POST['mdp']) && !empty($_POST['mdp'])) {
            $req = $bdd->prepare("SELECT * FROM `admin`"); 
            $req->execute();
            $results = $req->fetchAll();
            $identifiantInvalid = TRUE;
            foreach ($results as $membre) {
                if ($recupPseudo == $membre["pseudo"] && password_verify($recupMdp, $membre["mdp"])) {
                    $_SESSION['logged_in'] = true;
                    header("Location: index.php");
                } 
            }
      }
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log In Admin- Fếlés du bocal</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../index.php"><img class="w-25" src="../items/logo.png" alt="Logo Bocal"></a>  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Pseudo" name="pseudo">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" name="mdp">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" name="submit" class="btn btn-primary btn-block">Connexion</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>

  <?php

  if ($identifiantInvalid == true) {
    echo "<p id='erreur'>L'identifiant ou le mot de passe n'est pas valide</p>";
  }
   
  ?>

</div>
<!-- /.login-box -->

