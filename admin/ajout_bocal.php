<?php

session_start();

if (!isset($_SESSION["logged_in"])) { 
    header ("Location: login.php");
}

include "config2.php";

$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom']: "";
$recupPhoto = isset($_FILES["photo"]) ? microtime().$_FILES["photo"]["name"]: "";
$recupDescription = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description']: "";
$recupPrix = isset($_POST['prix']) && !empty($_POST['prix']) ? $_POST['prix']: "";
$recupCategorie = isset($_POST['categorie']) && !empty($_POST['categorie']) ? $_POST['categorie']: "";

if (isset($_POST['submit'])){
    if (isset($_POST['nom']) && !empty($_POST['nom']) 
    && isset($_POST['description']) && !empty($_POST['description'])  
    && isset($_POST['prix']) && !empty($_POST['prix']) 
    && isset($_POST['categorie']) && !empty($_POST['categorie'])
    && isset($_FILES['photo']) && !empty($_FILES['photo'])
    )   {      
        $req = $bdd->prepare("INSERT INTO bocal (nom, photo, descrip, prix, id_categorie) VALUES (?,?,?,?,?)"); 
        $req->execute([$recupNom, $recupPhoto, $recupDescription, $recupPrix, $recupCategorie]);
        move_uploaded_file($_FILES["photo"]["tmp_name"], "images/".$recupPhoto);
        header("Location: liste_bocaux.php");
            
        }
}

include "nav.html";

?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Ajout d'un bocal</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="form-group">
                    <label for="nomBocal">Nom</label>
                    <input type="text" name ="nom" class="form-control" id="exampleInputEmail1" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="photoBocal">Photo :</label>
                    <input type="file" name="photo"  id="exampleInputEmail1" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="descriptionBocal">Description</label>
                    <textarea  name="description" class="form-control" id="exampleInputEmail1" placeholder=""></textarea>
                  </div>
                  <div class="form-group">
                    <label for="prixBocal">Prix</label>
                    <input type="number" name="prix" class="form-control" id="exampleInputEmail1" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="categorieBocal">Catégorie</label>
                    <select name="categorie" class="form-control" id="exampleInputEmail1">
                      <?php
                        $req = $bdd->prepare("SELECT * FROM categorie"); 
                        $req->execute();
                        $results = $req->fetchAll();
                        foreach($results as $categorie) {
                            echo "<option value=".$categorie['id_categorie'].">".$categorie['libelle']."</option>";
                        }
                      ?>
                    </select>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Ajouter</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<?php

include "footer.html";

?>