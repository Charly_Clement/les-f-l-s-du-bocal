<?php

include "config2.php";

if (isset($_POST['submit'])){
  $req = $bdd->prepare("SELECT * FROM bocal");
  $req->execute();
  $results = $req->fetchALL();
  foreach ($results as $bocal) {
      $recupVisibilite = isset($_POST['visibilite_'.$bocal['id_bocal']]) && !empty($_POST['visibilite_'.$bocal['id_bocal']]) ? $_POST['visibilite_'.$bocal['id_bocal']]: "non";
          $req = $bdd->prepare("UPDATE bocal SET visibilite=? WHERE id_bocal=?"); 
          $req->execute([$recupVisibilite, intval($bocal['id_bocal'])]);
  }   
}

include "nav.html";

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <form role="form" method="post">
            <h1 class="m-0 text-dark">Liste des bocaux</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#ancre-entree">Entrée</a></li>
              <li class="breadcrumb-item"><a href="#ancre-plat">Plat</a></li>
              <li class="breadcrumb-item"><a href="#ancre-dessert">Dessert</a></li>
              <li class="breadcrumb-item"><a href="#ancre-formule">Formule</a></li>
              <li class="breadcrumb-item"><a href="#ancre-epicerie">Epicerie</a></li>
            </ol>
          </div><!-- /.col -->   
        </div><!-- /.row -->
        <div><a href="ajout_bocal.php" style="background-color:#00bd49; color:white; text-align:center; padding:5px 50px; font-size:20px; border-radius:5px;">Ajouter un bocal</a></div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <button type="submit" name="submit" class="btn btn-primary" style="font-size:20px;">Validation des bocaux cochés</button>

        <!-- Small boxes (Stat box) -->
        <h3 id="ancre-entree">ENTREE</h3>
        <div class="row">

          <?php
          $req = $bdd->prepare("SELECT * FROM bocal WHERE id_categorie=1 ORDER BY nom");
          $req->execute();
          $results = $req->fetchALL();
          foreach ($results as $bocal) {
          ?>
          <div class="card" style="width: 31rem; margin: 10px;">
            <img src="images/<?php echo $bocal["photo"]; ?>" class="card-img-top" alt="image" style="height:350px;">
            <div class="card-body">
              <h5 class="card-title" style="font-size:25px; margin-bottom:10px;"><?php echo $bocal["nom"]; ?>&nbsp;&nbsp;<input type="checkbox" name="visibilite_<?php echo $bocal['id_bocal'];?>" value="oui"></h5>
              <p class="card-text"><?php echo $bocal["descrip"]; ?></p>
              <p class="card-text" style="font-size:30px; color:red;"><?php echo $bocal["prix"]; ?> €</p>
              <a href="modifier_bocal.php?id=<?php echo $bocal["id_bocal"] ?>" class="btn btn-success">Modifier</a>
            </div>
          </div>
          <?php }?>

        </div>
        <!-- /.row (main row) -->

        <h3 id="ancre-plat">PLAT</h3>
        <div class="row">

          <?php
          $req = $bdd->prepare("SELECT * FROM bocal WHERE id_categorie=2 ORDER BY nom");
          $req->execute();
          $results = $req->fetchALL();
          foreach ($results as $bocal) {
          ?>
          <div class="card" style="width: 31rem; margin: 10px;">
            <img src="images/<?php echo $bocal["photo"]; ?>" class="card-img-top" alt="image" style="height:350px;">
            <div class="card-body">
              <h5 class="card-title" style="font-size:25px; margin-bottom:10px;"><?php echo $bocal["nom"]; ?>&nbsp;&nbsp;<input type="checkbox" name="visibilite_<?php echo $bocal['id_bocal'];?>" value="oui"></h5>
              <p class="card-text"><?php echo $bocal["descrip"]; ?></p>
              <p class="card-text" style="font-size:30px; color:red;"><?php echo $bocal["prix"]; ?> €</p>
              <a href="modifier_bocal.php?id=<?php echo $bocal["id_bocal"] ?>" class="btn btn-success">Modifier</a>
            </div>
          </div>
          <?php }?>

        </div>
        <!-- /.row (main row) -->

        <h3 id="ancre-dessert">DESSERT</h3>
        <div class="row">

          <?php
          $req = $bdd->prepare("SELECT * FROM bocal WHERE id_categorie=3 ORDER BY nom");
          $req->execute();
          $results = $req->fetchALL();
          foreach ($results as $bocal) {
          ?>
          <div class="card" style="width: 31rem; margin: 10px;">
            <img src="images/<?php echo $bocal["photo"]; ?>" class="card-img-top" alt="image" style="height:350px;">
            <div class="card-body">
              <h5 class="card-title" style="font-size:25px; margin-bottom:10px;"><?php echo $bocal["nom"]; ?>&nbsp;&nbsp;<input type="checkbox" name="visibilite_<?php echo $bocal['id_bocal'];?>" value="oui"></h5>
              <p class="card-text"><?php echo $bocal["descrip"]; ?></p>
              <p class="card-text" style="font-size:30px; color:red;"><?php echo $bocal["prix"]; ?> €</p>
              <a href="modifier_bocal.php?id=<?php echo $bocal["id_bocal"] ?>" class="btn btn-success">Modifier</a>
            </div>
          </div>
          <?php }?>

        </div>
        <!-- /.row (main row) -->
        <h3 id="ancre-formule">FORMULE</h3>
        <div class="row">

          <?php
          $req = $bdd->prepare("SELECT * FROM bocal WHERE id_categorie=4 ORDER BY nom");
          $req->execute();
          $results = $req->fetchALL();
          foreach ($results as $bocal) {
          ?>
          <div class="card" style="width: 31rem; margin: 10px;">
            <img src="images/<?php echo $bocal["photo"]; ?>" class="card-img-top" alt="image" style="height:350px;">
            <div class="card-body">
              <h5 class="card-title" style="font-size:25px; margin-bottom:10px;"><?php echo $bocal["nom"]; ?>&nbsp;&nbsp;<input type="checkbox" name="visibilite_<?php echo $bocal['id_bocal'];?>" value="oui"></h5>
              <p class="card-text"><?php echo $bocal["descrip"]; ?></p>
              <p class="card-text" style="font-size:30px; color:red;"><?php echo $bocal["prix"]; ?> €</p>
              <a href="modifier_bocal.php?id=<?php echo $bocal["id_bocal"] ?>" class="btn btn-success">Modifier</a>
            </div>
          </div>
          <?php }?>

        </div>
        <!-- /.row (main row) -->
        <h3 id="ancre-epicerie">EPICERIE</h3>
        <div class="row">

          <?php
          $req = $bdd->prepare("SELECT * FROM bocal WHERE id_categorie=5 ORDER BY nom");
          $req->execute();
          $results = $req->fetchALL();
          foreach ($results as $bocal) {
          ?>
          <div class="card" style="width: 31rem; margin: 10px;">
            <img src="images/<?php echo $bocal["photo"]; ?>" class="card-img-top" alt="image" style="height:350px;">
            <div class="card-body">
              <h5 class="card-title" style="font-size:25px; margin-bottom:10px;"><?php echo $bocal["nom"]; ?>&nbsp;&nbsp;<input type="checkbox" name="visibilite_<?php echo $bocal['id_bocal'];?>" value="oui"></h5>
              <p class="card-text"><?php echo $bocal["descrip"]; ?></p>
              <p class="card-text" style="font-size:30px; color:red;"><?php echo $bocal["prix"]; ?> €</p>
              <a href="modifier_bocal.php?id=<?php echo $bocal["id_bocal"] ?>" class="btn btn-success">Modifier</a>
            </div>
          </div>
          <?php }?>

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  </form>

  
<?php

include "footer.html";

?>