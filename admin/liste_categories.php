<?php

session_start();

if (!isset($_SESSION["logged_in"])) { 
    header ("Location: login.php");
}

include "config2.php";
include "nav.html";

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Liste des catégories</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div style="margin-bottom:25px;"><a href="ajout_categorie.php" style="background-color:#00bd49; color:white; text-align:center; padding:5px 50px; font-size:20px; border-radius:5px;">Ajouter une catégorie</a></div>
        <div class="row">
          <div class="col-md-5">
            <div class="card">
              <div class="card-body p-0">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Catégorie</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                        $req = $bdd->prepare("SELECT * FROM categorie");
                        $req->execute();
                        $results = $req->fetchALL();
                        foreach ($results as $categorie) { 
                    ?>
                    <tr>
                      <td><?php echo $categorie["id_categorie"]; ?></td>
                      <td><?php echo $categorie["libelle"]; ?></td>
                      <td style="text-align:center;"><a href="modifier_categorie.php?id=<?php echo $categorie["id_categorie"] ?>" class="btn btn-success">Modifier</a></td>
                    </tr>
                    <?php }?>                 
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php

include "footer.html";

?>
