<?php

session_start();

if (!isset($_SESSION["logged_in"])) { 
    header ("Location: login.php");
}

include "config2.php";

$req = $bdd->prepare("SELECT * FROM bocal");
$req->execute();
$nbBocal = $req -> rowcount();

$req = $bdd->prepare("SELECT * FROM avis");
$req->execute();
$nbAvis = $req -> rowcount();

$req = $bdd->prepare("SELECT * FROM categorie");
$req->execute();
$nbCategorie = $req -> rowcount();
         
include "nav.html";
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tableau de bord</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Accueil</a></li>
              <li class="breadcrumb-item active">Tableau de bord</li>
            </ol>
          </div><!-- /.col -->   
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $nbBocal;?></h3>
                <?php 
                if ($nbBocal <=1) {
                  echo "<p>Bocal</p>";
                } else {
                  echo "<p>Bocaux</p>";
                }
                ?>
              </div>
              <div class="icon">
                <i class="ion ion-beaker"></i>
              </div>
              <a href="liste_bocaux.php" class="small-box-footer">Liste des bocaux <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $nbAvis;?></h3>
                <?php 
                  echo "<p>Avis</p>";
                ?>
              </div>
              <div class="icon">
                <i class="ion ion-chatbubbles"></i>
              </div>
              <a href="liste_avis.php" class="small-box-footer">Liste des avis <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div> 

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $nbCategorie;?></h3>
                <?php 
                if ($nbCategorie <=1) {
                  echo "<p>Catégorie</p>";
                } else {
                  echo "<p>Catégories</p>";
                }
                ?>
              </div>
              <div class="icon">
                <i class="ion ion-pricetag"></i>
              </div>
              <a href="liste_categories.php" class="small-box-footer">Liste des catégories <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div> 

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php

include "footer.html";

?>
