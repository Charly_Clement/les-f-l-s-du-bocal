<?php

session_start();

if (!isset($_SESSION["logged_in"])) { 
    header ("Location: login.php");
}

include "config2.php";

$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom']: "";
$recupPhoto = isset($_FILES["photo"]) ? "images/".microtime().$_FILES["photo"]["name"]: "";
$recupDescription = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description']: "";
$recupPrix = isset($_POST['prix']) && !empty($_POST['prix']) ? $_POST['prix']: "";
$recupCategorie = isset($_POST['categorie']) && !empty($_POST['categorie']) ? $_POST['categorie']: "";

$recupIdBocal = isset($_GET["id"])?$_GET["id"] : "";

$supBocal = isset($_GET["sup"])?$_GET["sup"] : "";

if ($supBocal == 'ok') {
        $req = $bdd->prepare("DELETE FROM bocal WHERE id_bocal = ?" );
        $req->execute([$recupIdBocal]); 
        header("Location: liste_bocaux.php");
}

$req = $bdd->prepare("SELECT * FROM bocal
                         WHERE id_bocal = ?
                         ");
    $req->execute([$recupIdBocal]);
    $results = $req->fetchALL();
    $stockInfos = $results[0];

  if (isset($_POST['submit'])){
    if (isset($_FILES['photo']) && !empty($_FILES['photo'])) {

      $req = $bdd->prepare("UPDATE bocal SET photo=? WHERE id_bocal=?"); 
      $req->execute([$recupPhoto, $recupIdBocal]);
      move_uploaded_file($_FILES["photo"]["tmp_name"], $recupPhoto);
    }
  }


  if (isset($_POST['submit'])){
    if (isset($_POST['nom']) && !empty($_POST['nom']) 
    && isset($_POST['description']) && !empty($_POST['description'])  
    && isset($_POST['prix']) && !empty($_POST['prix']) 
    )   {      
        
        $req = $bdd->prepare("UPDATE bocal SET nom=?, descrip=?, prix=?, id_categorie=? WHERE id_bocal=?"); 
        $req->execute([$recupNom, $recupDescription, $recupPrix, $recupCategorie, $recupIdBocal]);
        header("Location: liste_bocaux.php");
            
        }
}

include "nav.html";

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Modification d'un bocal</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="form-group">
                    <label for="nomBocal">Nom</label>
                    <input type="text" name ="nom" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $stockInfos['nom']?>">
                  </div>
                  <div class="form-group">
                    <label for="photoBocal">Photo :</label>
                    <input type="file" name="photo"  id="exampleInputEmail1" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="descriptionBocal">Description</label>
                    <textarea  name="description" class="form-control" id="exampleInputEmail1" placeholder=""><?php echo $stockInfos['descrip']?></textarea>
                  </div>
                  <div class="form-group">
                    <label for="prixBocal">Prix</label>
                    <input type="number" name="prix" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $stockInfos['prix']?>">
                  </div>
                  <div class="form-group">
                    <label for="categorieBocal">Catégorie</label>
                    <select name="categorie" class="form-control" id="exampleInputEmail1">
                      <?php
                        $req = $bdd->prepare("SELECT * FROM categorie"); 
                        $req->execute();
                        $results = $req->fetchAll();
                        foreach($results as $categorie) {
                            echo "<option value=".$categorie['id_categorie'].">".$categorie['libelle']."</option>";
                        }
                      ?>
                    </select>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Modifier</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
      <div><a href="?sup=ok&id=<?php echo $stockInfos["id_bocal"] ?>" style="background-color:#007bff; color:white; text-align:center; padding:5px 50px; font-size:20px; border-radius:5px;">Supprimer ce Bocal</a></div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<?php

include "footer.html";

?>