<?php

session_start();

if (!isset($_SESSION["logged_in"])) { 
    header ("Location: login.php");
}

include "config2.php";

$recupIdCategorie = isset($_GET["id"])?$_GET["id"] : "";

$supCategorie = isset($_GET["sup"])?$_GET["sup"] : "";

$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom']: "";

if ($supCategorie == 'ok') {
    $req = $bdd->prepare("DELETE FROM categorie WHERE id_categorie = ?" );
    $req->execute([$recupIdCategorie]); 
    header("Location: liste_categories.php");
}

$req = $bdd->prepare("SELECT * FROM categorie
                         WHERE id_categorie = ?
                         ");
    $req->execute([$recupIdCategorie]);
    $results = $req->fetchALL();
    $stockInfos = $results[0];

if (isset($_POST['submit'])){
    if (isset($_POST['nom']) && !empty($_POST['nom']) 
    )   {      
        
        $req = $bdd->prepare("UPDATE categorie SET libelle=? WHERE id_categorie=?"); 
        $req->execute([$recupNom, $recupIdCategorie]);
        header("Location: liste_categories.php");
            
        }
}

include "nav.html"

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Modification d'une catégorie</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="nomBocal">Libellé</label>
                    <input type="text" name ="nom" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $stockInfos['libelle']?>">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Modifier</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
      <div><a href="?sup=ok&id=<?php echo $stockInfos["id_categorie"] ?>" style="background-color:#007bff; color:white; text-align:center; padding:5px 50px; font-size:20px; border-radius:5px;">Supprimer cette catégorie</a></div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<?php

include "footer.html";

?>