<?php

session_start();

if (!isset($_SESSION["logged_in"])) { 
    header ("Location: login.php");
}

include "config2.php";

$recupIdAvis = isset($_GET["id"])?$_GET["id"] : "";

$supAvis = isset($_GET["sup"])?$_GET["sup"] : "";

$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom']: "";
$recupTexte = isset($_POST['texte']) && !empty($_POST['texte']) ? $_POST['texte']: "";

if ($supAvis == 'ok') {
        $req = $bdd->prepare("DELETE FROM avis WHERE id_avis = ?" );
        $req->execute([$recupIdAvis]); 
        header("Location: liste_avis.php");
}

$req = $bdd->prepare("SELECT * FROM avis
                         WHERE id_avis = ?
                         ");
    $req->execute([$recupIdAvis]);
    $results = $req->fetchALL();
    $stockInfos = $results[0];

  if (isset($_POST['submit'])){
    if (isset($_POST['nom']) && !empty($_POST['nom']) 
    && isset($_POST['texte']) && !empty($_POST['texte'])  
    )   {      
        
        $req = $bdd->prepare("UPDATE avis SET nom_prenom=?, texte=? WHERE id_avis=?"); 
        $req->execute([$recupNom, $recupTexte, $recupIdAvis]);
        header("Location: liste_avis.php");
            
        }
}

include "nav.html";

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Modification d'un avis</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="form-group">
                    <label for="nomBocal">Nom</label>
                    <input type="text" name ="nom" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo $stockInfos['nom_prenom']?>">
                  </div>
                  <div class="form-group">
                    <label for="descriptionBocal">Description</label>
                    <textarea  name="texte" class="form-control" id="exampleInputEmail1" placeholder=""><?php echo $stockInfos['texte']?></textarea>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Modifier</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
      <div><a href="?sup=ok&id=<?php echo $stockInfos["id_avis"] ?>" style="background-color:#007bff; color:white; text-align:center; padding:5px 50px; font-size:20px; border-radius:5px;">Supprimer cet avis</a></div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php

include "footer.html";

?>
