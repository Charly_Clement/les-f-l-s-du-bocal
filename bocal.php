
<?php

    include 'nav.php';
    include 'config.php';

    $id_bocal = $_GET['id'];

    // Requête pour la présentation du produit
    $bocaux = $pdo->prepare("SELECT * FROM bocal WHERE id_bocal = ?");
    $bocaux->execute([$id_bocal]);
    $bocaux = $bocaux->fetchAll();

    // Requête pour sélectionner des recettes au hasard
    $recettes = $pdo->prepare("SELECT * FROM bocal ORDER BY RAND() LIMIT 4");
    $recettes->execute();
    $recettes = $recettes->fetchAll();

?>

    <div class="container-fluid fond-beige py-3">
        <div class="container pt-3">

            <!-- Fil d'ariane -->
            <div class="row align-items-center">
                <div class="col pl-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a class="mr-3 text-dark border border-dark p-2" href="bocaux.php">RETOUR</a><li>
                        <li class="breadcrumb-item">ACCUEIL</li>
                        <li class="breadcrumb-item">CATÉGORIE</li>
                        <li class="breadcrumb-item"><?php foreach ($bocaux as $categorie) { echo strtoupper($categorie['nom']); } ?></li>
                    </ol>
                </div><!-- / Col -->
            </div>

            <!-- Affichage de la présentation du produit -->
            <div class="row">
                <?php foreach ($bocaux as $bocal) { ?>
                    <div class="card mb-3 fond-beige border-0">
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <img src="admin/images/<?php echo $bocal['photo']; ?>" class="card-img img-presentation-bocal" alt="Bocal">
                            </div>
                            <div class="col-md-6">
                                <div class="card-body">
                                    <h4><?php  ?></h4>
                                    <h1 class="card-title font-weight-bold"><?php echo $bocal['nom']; ?></h1>
                                    <p class="card-text"><?php echo $bocal['descrip']; ?></p>
                                    <p class="prix prix-presentation-bocal"><?php echo $bocal['prix']; ?>€</p>
                                </div>
                            </div>
                        </div>
                    </div><!-- / Card mb-3 -->
                <?php } ?>
            </div><!-- / Row -->

        </div><!-- / Container -->
    </div><!-- / Container-fluid -->

    <!-- Affichage de recettes au hasard -->
    <div class="container py-4">
        <div class="row">
            <div class="col font-weight-bold">
                <h2>Vous pourrez aimez aussi</h2>
            </div>
        </div><!-- / row -->
        <div class="row">
            <?php foreach ($recettes as $recette) { ?>
                <div class="card m-2 border-0" style="width: 15rem;">
                    <img src="admin/images/<?php echo $recette['photo'] ?>" class="card-img-top radius" alt="Entrées">
                    <div class="card-body">
                        <p class="card-text"><b><?php echo $recette['nom'] ?></b></p>
                        <div class="d-flex justify-content-between">
                            <p class="prix"><?php echo $recette['prix'] ?>€</p>
                            <div class="je-decouvre">
                                <?php echo'<a href="bocal.php?id='.$recette['id_bocal'].'" class="nav-link text-white">VOIR LE PRODUIT</a>'; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div><!--  / row -->
    </div><!-- / container -->

<?php include 'footer.php'; ?>
