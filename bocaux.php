
<?php

    include 'nav.php';
    include 'config.php';

    // Sélectionne les Formules
    $formules = $pdo->prepare("SELECT * FROM bocal WHERE id_categorie=4
                                AND visibilite = 'oui'");
    $formules->execute();
    $formules = $formules->fetchAll();

    // Sélectionne les Entrées
    $entrees = $pdo->prepare("SELECT * FROM bocal WHERE id_categorie=1
                                AND visibilite = 'oui'");
    $entrees->execute();
    $entrees = $entrees->fetchAll();

    // Sélectionne les Plats
    $plats = $pdo->prepare("SELECT * FROM bocal WHERE id_categorie=2
                                AND visibilite = 'oui'");
    $plats->execute();
    $plats = $plats->fetchAll();

    // Sélectionne les Desserts
    $desserts = $pdo->prepare("SELECT * FROM bocal WHERE id_categorie=3
                                AND visibilite = 'oui'");
    $desserts->execute();
    $desserts = $desserts->fetchAll();

    // Sélectionne les Épiceries
    $epiceries = $pdo->prepare("SELECT * FROM bocal WHERE id_categorie=5
                                AND visibilite = 'oui'");
    $epiceries->execute();
    $epiceries = $epiceries->fetchAll();

?>

    <div class="container-fluid fond-beige pb-5">
        <div class="container">
            <div class="row py-4">
                <div class="col-4 pl-0">
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a class="text-dark" href="index.php">ACCUEIL</a></li>
                        <li class="breadcrumb-item active" aria-current="page">BOUTIQUE</li>
                    </ol>
                </div>
                <div class="col-8 pr-0">
                    <ul class="nav justify-content-center my-auto">
                        <li class="nav-item nav-radius mx-2">
                            <a class="nav-link text-dark ml-2">FORMULES</a>
                        </li>
                        <li class="nav-item nav-radius mx-2">
                            <a class="nav-link text-dark ml-2">ENTRÉES</a>
                        </li>
                        <li class="nav-item nav-radius mx-2">
                            <a href="#plats" class="nav-link text-dark ml-2" href="#">PLATS</a>
                        </li>
                        <li class="nav-item nav-radius mx-2">
                            <a href="#desserts" class="nav-link text-dark ml-2">DESSERTS</a>
                        </li>
                        <li class="nav-item nav-radius mx-2">
                            <a href="#epicerie" class="nav-link text-dark ml-2">ÉPICERIE</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!--Affichage des Formules -->
        <div class="container">
            <div class="row">
                <h2>Formules</h2>
            </div>
            <div class="row">
                <?php foreach ($formules as $formule) { ?>
                <div class="col-6 pl-0">
                    <div class="card fond-beige border-0">
                        <img src="admin/images/<?php echo $formule['photo'] ?>" class="card-img-top img-formule" alt="Formule">
                        <div class="card-body">
                            <p class="card-title"><?php echo $formule['nom'] ?></p>
                            <div class="d-flex justify-content-between">
                                <p class="prix"><?php echo $formule['prix'] ?></p>
                                <div class="je-decouvre">
                                    <a href="#" class="nav-link text-white">VOIR LE PRODUIT</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div><!-- / Row -->
        </div>
    </div>
    <!-- / Formules -->

    <div class="container">

        <!-- Entrées -->
        <div class="row mt-5">
            <h2><b>Entrées</b></h2>
        </div>
        <div class="row">
            <?php foreach ($entrees as $entree) { ?>
                <div class="card m-2 border-0" style="width: 15rem;">
                    <img src="admin/images/<?php echo $entree['photo'] ?>" class="card-img-top radius" alt="Entrées">
                    <div class="card-body">
                        <p class="card-text"><b><?php echo $entree['nom'] ?></b></p>
                        <div class="d-flex justify-content-between">
                            <p class="prix"><?php echo $entree['prix'] ?>€</p>
                            <div class="je-decouvre">
                                <?php echo'<a href="bocal.php?id='.$entree['id_bocal'].'" class="nav-link text-white">VOIR LE PRODUIT</a>'; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div><!--  / row -->
        <!-- / Entrées -->

        <!-- Plats -->
        <div id="plats" class="row mt-5">
            <h2><b>Plats</b></h2>
        </div>
        <div class="row">
            <?php foreach ($plats as $plat) { ?>
                <div class="card m-2 border-0" style="width: 15rem;">
                    <img src="admin/images/<?php echo $plat['photo'] ?>" class="card-img-top radius" alt="Entrées">
                    <div class="card-body">
                        <p class="card-text"><b><?php echo $plat['nom'] ?></b></p>
                        <div class="d-flex justify-content-between">
                            <p class="prix"><?php echo $plat['prix'] ?>€</p>
                            <div class="je-decouvre">
                                <?php echo'<a href="bocal.php?id='.$plat['id_bocal'].'" class="nav-link text-white">VOIR LE PRODUIT</a>'; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div><!--  / row -->
        <!-- / Plats -->

        <!-- Desserts -->
        <div id="desserts" class="row mt-5">
            <h2><b>Desserts</b></h2>
        </div>
        <div class="row">
            <?php foreach ($desserts as $dessert) { ?>
                <div class="card m-2 border-0" style="width: 15rem;">
                    <img src="admin/images/<?php echo $dessert['photo'] ?>" class="card-img-top radius" alt="Entrées">
                    <div class="card-body">
                        <p class="card-text"><b><?php echo $dessert['nom'] ?></b></p>
                        <div class="d-flex justify-content-between">
                            <p class="prix"><?php echo $dessert['prix'] ?>€</p>
                            <div class="je-decouvre">
                                <?php echo'<a href="bocal.php?id='.$dessert['id_bocal'].'" class="nav-link text-white">VOIR LE PRODUIT</a>'; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div><!--  / row -->
        <!-- / Desserts -->

        <!-- Épicerie -->
        <div id="epicerie" class="row mt-5">
            <h2><b>Épicerie</b></h2>
        </div>
        <div class="row">
            <?php foreach ($epiceries as $epicerie) { ?>
                <div class="card m-2 border-0" style="width: 15rem;">
                    <img src="admin/images/<?php echo $epicerie['photo'] ?>" class="card-img-top radius" alt="Entrées">
                    <div class="card-body">
                        <p class="card-text"><b><?php echo $epicerie['nom'] ?></b></p>
                        <div class="d-flex justify-content-between">
                            <p class="prix"><?php echo $epicerie['prix'] ?>€</p>
                            <div class="je-decouvre">
                                <?php echo'<a href="bocal.php?id='.$epicerie['id_bocal'].'" class="nav-link text-white">VOIR LE PRODUIT</a>'; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div><!--  / row -->
        <!-- / Épicerie -->

    </div><!--  / container -->

<?php include 'footer.php'; ?>