<?php 

include 'nav.php'; 
include 'config.php';

/* traitement du formulaire des avis (partage d'expérience) */
$recupNomavis = isset($_POST['nom-avis']) && !empty($_POST['nom-avis']) ? ($_POST['nom-avis']) : ""; 
$recupTexteavis = isset($_POST['texte-avis']) && !empty($_POST['texte-avis']) ? ($_POST['texte-avis']) : ""; 

if(isset($_POST['submit'])){
    if($recupNomavis !="" && $recupTexteavis !="" ){
        try{
            $req = $pdo->prepare("INSERT INTO avis (nom_prenom, texte, date_creation) VALUES (:nom_prenom, :texte, NOW());");
            $req ->execute([
                'nom_prenom'=>$recupNomavis, 
                'texte'=>$recupTexteavis,
                ]);
            header("Location: contact.php");
        }catch(PDO_Exception $x){
            echo "Erreur insert into ".$x->getMessage();
        }
    }else{
        echo "Veuillez remplir tous les champs";
    }
}


/* traitement du formulaire de contact */
$recupPrenom = isset($_POST['prenom']) && !empty($_POST['prenom']) ? ($_POST['prenom']) : ""; 
$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? ($_POST['nom']) : ""; 
$recupMail = isset($_POST['mail']) && !empty($_POST['mail']) ? ($_POST['mail']) : ""; 
$recupTel = isset($_POST['tel']) && !empty($_POST['tel']) ? ($_POST['tel']) : ""; 
$recupObjet = isset($_POST['objet']) && !empty($_POST['objet']) ? ($_POST['objet']) : ""; 
$recupMessage = isset($_POST['message']) && !empty($_POST['message']) ? ($_POST['message']) : ""; 

if(isset($_POST['submit1'])){
    if($recupPrenom !="" && $recupNom !="" && $recupMail !="" && $recupTel !="" && $recupObjet !="" && $recupMessage !=""){
        try{
            $req = $pdo->prepare("INSERT INTO contact (prenom, nom, mail, telephone, objet, texte, date_creation) VALUES (:prenom, :nom, :mail, :tel, :objet, :message, NOW());");
            $req ->execute([
                'prenom'=>$recupPrenom, 
                'nom'=>$recupNom,
                'mail'=>$recupMail, 
                'tel'=>$recupTel,
                'objet'=>$recupObjet,
                'message'=>$recupMessage,
                ]);
            header("Location: contact.php");
        }catch(PDO_Exception $x){
            echo "Erreur insert into ".$x->getMessage();
        }
    }else{
        echo "Veuillez remplir tous les champs";
    }
}


?>

<section id="avis">
    <div>
        <h2>Ils se sont régalés</h2><hr>
        <h5>Partager votre expérience</h5>
        <form action="" method="post">
            <input type="text" name="nom-avis" placeholder="Votre Nom & Prénom"><br/>
            <textarea name="texte-avis" id="" cols="30" rows="5" placeholder="Votre expérience"></textarea>
            <div>
                <input class="submit" type="submit" name="submit" value="PARTAGER">
            </div>
        </form>
    </div>
    <div class="card test">

    <?php
    $req = $pdo->prepare("SELECT * FROM avis WHERE visibilite ='oui' LIMIT 3");
            $req -> execute();
            $result = $req -> fetchALL();
            foreach ($result as $value) { ?>
                <div class="card-body mb-2">
                    <hr>
                    <blockquote class="blockquote mb-0">
                    <p><?php echo htmlspecialchars($value['texte']);?></p>
                    <footer class="blockquote-footer"><?php echo htmlspecialchars($value['nom_prenom']).' -- envoyé le : '.htmlspecialchars($value['date_creation']);?></footer>
                    </blockquote>
                    <hr>
                </div>
            <?php } ?>
    </div>
 </section>

<div id="horaires">
    <h2>Nos horaires</h2>
    <div id="cartes">
        <div class="card choix" style="width: 18rem;">
            <div class="pic">
                <svg xmlns="http://www.w3.org/2000/svg" width="49.754" height="63.038" viewBox="0 0 49.754 63.038"><defs><style>.a,.b{fill:#172144;}.b{fill-rule:evenodd;}</style></defs><g transform="translate(-869 -4233.285)"><g transform="translate(869 4233.285)"><g transform="translate(1.102 0)"><path class="a" d="M3.437,0A1.955,1.955,0,0,0,1.5,1.937V5.779A1.951,1.951,0,0,0,3.437,7.712H32.618A1.948,1.948,0,0,0,34.55,5.779V1.937A1.951,1.951,0,0,0,32.618,0ZM3.7,2.2H32.347V5.508H3.7Z" transform="translate(0.703 0)"></path><path class="b" d="M6.008,2.5a1.1,1.1,0,0,0-1.1,1.1v1c-.2,1.952-2.043,3.585-3.563,5.883a5.982,5.982,0,0,0-.844,3.4V51.21a9.425,9.425,0,0,0,3.77,7.542l.033.026a6.539,6.539,0,0,0,3.75,1h22.3a7.358,7.358,0,0,0,4.411-1.467l.029-.022a9.072,9.072,0,0,0,3.151-7.064V13.9a6.879,6.879,0,0,0-.8-3.353c-1.377-2.43-3.184-4.18-3.6-6.042V3.6a1.1,1.1,0,0,0-1.1-1.1H6.008Zm1.1,2.2H31.347a1.04,1.04,0,0,0,.026.234c.6,2.8,2.7,4.667,3.856,6.7a7.25,7.25,0,0,1,.516,2.234V51.2a6.92,6.92,0,0,1-2.316,5.356,5.151,5.151,0,0,1-3.071,1.025H8.108a4.361,4.361,0,0,1-2.527-.6A7.25,7.25,0,0,1,2.7,51.21V13.889a4.062,4.062,0,0,1,.478-2.181C4.455,9.782,6.8,7.885,7.106,4.818c0-.037,0-.075,0-.112Z" transform="translate(-0.5 3.006)"></path><path class="a" d="M17.4,15.494a1.1,1.1,0,0,0-1.084,1.115l0,14.868a7.233,7.233,0,0,1-1.441,4.334c-.542.72-1.624,2.164-1.624,2.164a1.1,1.1,0,1,0,1.694,1.41c.022-.029.046-.057.066-.088l1.622-2.164a9.42,9.42,0,0,0,1.886-5.656l0-14.868a1.1,1.1,0,0,0-1.088-1.115Z" transform="translate(14.531 18.645)"></path></g><path class="a" d="M48.652,29.813H1.1a1.1,1.1,0,1,1,0-2.2h47.55a1.1,1.1,0,1,1,0,2.2Z" transform="translate(0 33.224)"></path></g></g></svg>				</span>
            </div>
            <div class="card-body">
            <h5 class="card-title">Sur place & à emporter </h5>
                <p class="card-text">Vous souhaitez organiser un cocktail dinatoire, une soirée d’entreprise, un anniversaire… <br/>Parlons-en.</p>
            </div>
        </div>
        <div class="card choix" style="width: 18rem;">
            <div class="pic">
                <svg xmlns="http://www.w3.org/2000/svg" width="63.904" height="54.6" viewBox="0 0 63.904 54.6"><defs><style>.a{fill:#162144;}</style></defs><path class="a" d="M60,17.7H55.5L50.4,5a4.439,4.439,0,0,0-4.1-2.8H41.8a3.121,3.121,0,0,0-3-2.2H25.2a3.121,3.121,0,0,0-3,2.2H17.7A4.439,4.439,0,0,0,13.6,5l-5,12.7H4a4.006,4.006,0,0,0-.3,8L9.6,51.4a4.1,4.1,0,0,0,4.1,3.2H50.2a4.179,4.179,0,0,0,4.1-3.2l5.9-25.7a3.891,3.891,0,0,0,3.7-4A3.865,3.865,0,0,0,60,17.7ZM25.2,2H38.8A1.155,1.155,0,0,1,40,3.2a1.155,1.155,0,0,1-1.2,1.2H25.2A1.155,1.155,0,0,1,24,3.2,1.155,1.155,0,0,1,25.2,2ZM15.3,5.7a2.526,2.526,0,0,1,2.3-1.5h4.5a3.121,3.121,0,0,0,3,2.2H38.7a3.121,3.121,0,0,0,3-2.2h4.5a2.526,2.526,0,0,1,2.3,1.5l4.8,12H10.6ZM52.4,50.9a2.14,2.14,0,0,1-2.1,1.7H13.7a2.14,2.14,0,0,1-2.1-1.7L5.8,25.7H58.2ZM60,23.7H4a2,2,0,0,1,0-4H60a2,2,0,0,1,0,4Z"></path><path class="a" d="M49.2,29.9a1.028,1.028,0,0,0-1.2.7L43.9,47.2a.982.982,0,1,0,1.9.5L50,31.1A1.012,1.012,0,0,0,49.2,29.9Z"></path><path class="a" d="M37.7,29.9a1.088,1.088,0,0,0-1.1.9L35.2,47.4a1.088,1.088,0,0,0,.9,1.1h.1a.987.987,0,0,0,1-.9L38.6,31A.9.9,0,0,0,37.7,29.9Z"></path><path class="a" d="M26.2,29.9a.9.9,0,0,0-.9,1.1l1.4,16.6a.987.987,0,0,0,1,.9h.1a.9.9,0,0,0,.9-1.1L27.3,30.8A.9.9,0,0,0,26.2,29.9Z"></path><path class="a" d="M16,30.6a.982.982,0,1,0-1.9.5l4.1,16.6a.982.982,0,1,0,1.9-.5Z"></path></svg>				</span>
            </div>
            <div class="card-body">
            <h5 class="card-title">Ouverture en soirée</h5>
                <p class="card-text">Vous souhaitez organiser un cocktail dinatoire, une soirée d’entreprise, un anniversaire… <br/>Parlons-en.</p>
            </div>
        </div>
        <div class="card choix" style="width: 18rem;">
            <div class="pic">
    			<svg xmlns="http://www.w3.org/2000/svg" width="64" height="47.994" viewBox="0 0 64 47.994"><defs><style>.a{fill:#172144;}</style></defs><g transform="translate(0 -8.003)"><path class="a" d="M51,27.009a12.991,12.991,0,0,0-3.023.369L42.373,10h5.348A1.993,1.993,0,0,1,46,11a1,1,0,0,0,0,2,4,4,0,0,0,4-4,1,1,0,0,0-1-1H41a.875.875,0,0,0-.154.031.969.969,0,0,0-.152.017c-.015.005-.024.017-.039.021a.955.955,0,0,0-.184.1.885.885,0,0,0-.147.1.936.936,0,0,0-.117.149,1.006,1.006,0,0,0-.106.157,1.118,1.118,0,0,0-.049.17.922.922,0,0,0-.042.209c0,.015-.009.027-.009.042a.915.915,0,0,0,.03.149.932.932,0,0,0,.018.157l4.419,13.7H25.945l-.6-2H30a1,1,0,0,0,1-1,4,4,0,0,0-4-4H20a1,1,0,0,0-1,1v3a1,1,0,0,0,1,1h3.256l.837,2.788-4.1,5.258V28a5.006,5.006,0,0,0-5-5,4.947,4.947,0,0,0-2,.424,4.947,4.947,0,0,0-2-.424,5.006,5.006,0,0,0-5,5v3H1a1,1,0,0,0-1,1V55a1,1,0,0,0,1,1H25a1,1,0,0,0,1-1V41.009h.143a3.955,3.955,0,0,0,5.877,2.434L32.58,44H32a1,1,0,0,0,0,2h5a1,1,0,0,0,0-2H35.407L33.432,42.03a3.883,3.883,0,0,0-.008-4.057L45.393,25.882l.679,2.1A12.986,12.986,0,1,0,51,27.009Zm-22.277-8H21v-1h6A2,2,0,0,1,28.723,19.009Zm14.719,6L32,36.567a3.948,3.948,0,0,0-2-.558c-.052,0-.1.013-.149.015L26.545,25.009ZM17.994,28v3h-2V28a4.955,4.955,0,0,0-1.023-3l.023,0A3,3,0,0,1,17.994,28Zm-6,3V28a2.985,2.985,0,0,1,1-2.22,2.985,2.985,0,0,1,1,2.22v3Zm-4-3a3,3,0,0,1,3-3l.023,0a4.948,4.948,0,0,0-1.023,3v3h-2ZM2,33H20V54H2ZM24,54H22V33h2Zm1-23H21.015L24.8,26.147,27.935,36.6a4,4,0,0,0-1.793,2.408H26V32a1,1,0,0,0-1-1Zm5.708,8.3a1,1,0,1,0-1.414,1.414l1.217,1.217a1.952,1.952,0,0,1-.51.074,2,2,0,1,1,2-2,1.975,1.975,0,0,1-.075.513ZM51,51.009a11,11,0,0,1-4.314-21.116l.31.959a10.02,10.02,0,1,0,1.9-.616l-.307-.952A11,11,0,1,1,51,51.009ZM49.52,38.677l.528,1.639a1,1,0,0,0,.952.693,1.024,1.024,0,0,0,.307-.048,1,1,0,0,0,.646-1.259l-.531-1.646A2,2,0,1,1,49,40.009,1.979,1.979,0,0,1,49.52,38.677ZM51,44.009a4,4,0,0,0,0-8c-.079,0-.153.019-.231.023l-1.252-3.88a8.03,8.03,0,1,1-1.9.619l1.248,3.868A3.993,3.993,0,0,0,51,44.009Z"></path><path class="a" d="M10,36a1,1,0,0,0-1,1v1h-.5V37a1,1,0,0,0-2,0v1H6V37a1,1,0,0,0-2,0v4s0,0,0,.005a2,2,0,0,0,2,2h.5v7a1,1,0,0,0,2,0V43h.489l.01,0a2,2,0,0,0,2-2h0V37a1,1,0,0,0-1-1ZM9,40v.994s0,.005,0,.008H6V40Z"></path><path class="a" d="M14.005,36h-.012a1,1,0,0,0-1,1c0,.011.006.019.006.029V50a1,1,0,0,0,2,0V46h2a1,1,0,0,0,1-1V40A4.01,4.01,0,0,0,14.005,36ZM15,38.279a2,2,0,0,1,1,1.726V44H15Z"></path></g></svg>				</span>
            </div>
            <div class="card-body">
                <h5 class="card-title">Livraison (prochainement...)</h5>
                <p class="card-text">Faites-nous d'ores et déjà part de vos besoins. Nous ferons tout notre possible pour y répondre.</p>
            </div>
        </div>
    </div>
    <div class="je-commande mt-3 horaires">
        <a href="#" class="nav-link text-white"><i class="fas fa-phone-alt"></i> JE COMMANDE</a>
    </div>
</div>

<section id="geo">
    <div id="adresse">
        <h2>Géo- <br>bocalisation</h2>
        <p>Latitude : 47.995919 <br />Longitude : 0.202093</p>
        <p>02 43 78 25 11 <br />contact@les-feles-du-bocal.bio</p>
        <p>198 rue Nationale <br />Place Washington <br />72000 Le Mans</p>
    </div>
    <div id="map">
        <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="100%" height="300px" src="https://maps.google.com/maps?q=198%20rue%20nationale%2C%2072100%20Le%20Mans&amp;t=m&amp;z=15&amp;output=embed&amp;iwloc=near" aria-label="198 rue nationale, 72100 Le Mans"></iframe>
    </div>
</section>

<section id="formulaire">
    <h2>Nous contacter</h2>
    <form action="" method="post">
        <div id="coordonnees">
            <div class="contact">
                <input type="text" name="prenom" placeholder="Prénom">
                <input type="text" name="nom" placeholder="Nom">
                <div>
                    <input  class="mailtel" type="mail" name="mail" placeholder="Adresse mail">
                    <input  class="mailtel" type="text" name="tel" placeholder="Téléphone">
                </div>
            </div>
            <div class="contact">
                <input class="mailtel" type="text" name="objet" placeholder="Objet">
                <textarea class="mailtel" name="message" id="" cols="30" rows="3" placeholder="Message"></textarea>
            </div>
        </div>
        <div>
            <input class="submit" type="submit" name="submit1" value="ENVOYER">
        </div>
    </form>
</section>

<?php include 'footer.php'; ?>
