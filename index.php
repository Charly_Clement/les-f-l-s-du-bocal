
<?php include 'nav.php'; ?>

    
    <div class="container-fluid fond-beige">
        
        <header>
            <!-- Jumbotron -->
            <div class="row py-5 img-home">
                <div class="container-fluid w-75">
                    <div class="jumbotron text-white w-100">
                        <div class="col-4 offset-1">
                        <h1><b>
                            Bocaux<br>
                            Locaux</b>
                        </h1>
                        <p>
                            Le restaurant Les Fêlés du Bocal vous<br>
                            propose des produits frais , locaux, servis<br>
                            dans des bocaux.
                        </p>
                        <p><b>
                            Restauration Bio | Sur place, à emporter<br>
                            et prochainement en livraison
                        <b></p>

                        <div class="je-decouvre text-center w-50">
                            <a href="bocaux.php" class="nav-link text-white">JE DÉCOUVRE</a>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / Jumbotron -->
        </header>

        <!-- Carte -->
        <section>
            <div class="row text-center">
                <div class="container">
                    <div class="card-group">
                        <div class="card mx-2">
                            <div class="card-body p-4">
                                <h5 class="card-title"><b>Du bon, du bio, du bocal</b></h5>
                                <p class="card-text">
                                    En verre et pour le goût, le chois de la transparence pour une 
                                    conservation optimale des produits et de leurs saveurs. Et puis c'est 
                                    bien connu, plaisir des pupilles appelle bonheur des papilles.
                                </p>
                            </div>
                        </div>
                        <div class="card mx-2">
                            <div class="card-body p-4">
                                <h5 class="card-title"><b>Qualité 1 - 0 Déchet</b></h5>
                                <p class="card-text">
                                    Parce que ça peut matcher, nous misons sur la victoire de la gourmandise 
                                    contre le gaspillage. Oui, si les bocaux nous emballent pour leurs 
                                    qualités, c'est aussi parce qu'ils nous épargne quantité d'emballage.
                                </p>
                            </div>
                        </div>
                        <div class="card mx-2">
                            <div class="card-body p-4">
                                <h5 class="card-title"><b>Une seule consigne</b></h5>
                                <p class="card-text">
                                    Prolonger le plaisr gustatif. Échanger avec le chef, toujours friand 
                                    de retours sur sa cuisine artisanale.<br> Accessoirement:<br> 1 bocal 
                                    restitué = 1€<br> à valoir sur votre prochain repas.
                                </p>
                            </div>
                        </div>
                    </div><!-- / card-group -->
                </div><!-- / container -->
            </div><!-- / row -->
        </section>
        <!-- / Carte -->

        <!-- Au menu aujourd'hui -->
        <section>
            <div class="container mt-5">
                <div class="row">
                    <div class="col-4">
                        <h2><b>Au menu<br> aujourd'hui</b></h2>
                        <p>
                            Entre le chef et vous, il n'y a pas qu'une vitre. Celle de votre écran.
                             Parce qu'une image vaut mille mots et parce que vous n'avez pas froid aux yeux, 
                             retrouvez ici au quotidien l'insta-show des FFêlés du Bocal : 
                             toutes les recettes et menus du jour.
                        </p>
                        <div class="je-decouvre text-center w-75">
                            <a href="#" class="nav-link text-white">JE DÉCOUVRE LES RECETTES</a>
                        </div>
                    </div>
                    <div class="col-6 offset-1">
                        <div>
                            <img class="img-responsive imagehome" src="items/imagehome.jpg" alt="Imagehome">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- / Au menu aujourd'hui -->

    </div><!-- / container-fluid -->

        <!-- Il se sont régalés -->
        <section>
            <div class="container">
                <div class="row pt-5 align-items-center">
                    <div class="col-6">
                        <div>
                            <h2><b>Ils se sont régalés</b></h2>
                        </div>
                        <div class="je-decouvre text-center w-50">
                            <a href="#" class="nav-link text-white">VOIR TOUS LES AVIS</a>
                        </div>
                    </div>
                    <div class="col-6">
                    <i class="fas fa-quote-right"></i>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus dolorem deleniti debitis cum numquam aspernatur, natus voluptas vitae alias molestiae! Ipsam sed quam impedit voluptatibus perspiciatis in similique libero veritatis.
                    </p>
                    <p>Nom du client</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- / Il se sont régalés -->

        <!-- Suivez-nous sur : -->
        <section>
            <div class="container-fluid fond-bleu ">
                <div class="row text-center">
                    <div class="col">
                        <h2><b>Suivez-nous sur :</b></h2>
                        <i class="fab fa-2x fa-instagram mx-2"></i>
                        <i class="fab fa-2x fa-facebook-square mx-2"></i>
                    </div>
                </div>
            </div>
        </section>
        <!-- / Suivez-nous sur : -->

<?php include 'footer.php'; ?>
